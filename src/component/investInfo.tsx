import React, { useEffect, useState } from "react";
import Paper from "@material-ui/core/Paper";
import Pagination from "@material-ui/lab/Pagination";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import { Box } from "@material-ui/core";
import { useStyles } from "./componentStyle";
import Grid from "@material-ui/core/Grid";
import { useFindInvestmentListQuery } from "../generated/graphql";
import Base64 from "base-64";
import LinktoCompanyPage from "./LinktoCompanyPage";
import LinktoFundedType from "./LinktoFundedType";

interface InvestInfoProps {
  id: string;
}

interface InvestInfoData {
  investCompanyID: string,
  investCompanyName: string,
  fundedCompanyID: string,
  fundedCompanyName: string,
  categoryCode: string;
  fundingRoundType: string;
  fundedAt: string;
  raisedAmountUsd: number;
}

function createInvestmentData(
  investCompanyID: string,
  investCompanyName: string,
  fundedCompanyID: string,
  fundedCompanyName: string,
  categoryCode: string,
  fundingRoundType: string,
  fundedAt: string,
  raisedAmountUsd: number
) {
  return {
    investCompanyID,
    investCompanyName,
    fundedCompanyID,
    fundedCompanyName,
    categoryCode,
    fundingRoundType,
    fundedAt,
    raisedAmountUsd,
  };
}

export default function InvestInfo(props: InvestInfoProps) {
  const classes = useStyles();
  const [rows, setRows] = useState<InvestInfoData[]>([]);
  const [newAfter, setNewAfter] = useState(0);
  const [page, setPage] = React.useState(1);
  const [totalPageCount, setTotalPageCount] = useState(0);
  const [rounds, setRounds] = useState(0);
  const [isExist, setIsExist] = useState(true)
  const handleChange = (event: React.ChangeEvent<unknown>, value: number) => {
    setPage(value);
  };
  const [
    findInvestmentListQueryResult,
    executeFindInvestmentListQuery,
  ] = useFindInvestmentListQuery({
    variables: {
      first: 3,
      after: newAfter,
      companyID: Base64.decode(props.id),
    },
    pause: false,
  });

  useEffect(() => {
    setTimeout(() => {
      setNewAfter((page - 1) * 3);
    }, 0);
  }, [page]);

  useEffect(() => {
    let nodes =
      findInvestmentListQueryResult.data?.QueryInvestmentListByCompanyID?.nodes;
    let res = nodes?.map((value) => {
      let newInvestmentInfo = createInvestmentData(
        value?.investorInfo?.companyID as string,
        value?.investorInfo?.companyName as string,
        value?.financingInfo?.companyID as string,
        value?.financingInfo?.companyName as string,
        value?.investorInfo?.categoryCode as string,
        value?.financingInfo?.fundingRoundType as string,
        value?.financingInfo?.fundedAt as string,
        value?.financingInfo?.raisedAmountUsd as number
      );
      return newInvestmentInfo;
    });


    while (res?.length as number < 5) {
      res?.push(createInvestmentData("", "", "", "", "", "", "", 0))
    }

    while (res?.length as number > 5 && res?.length as number < 10) {
      res?.push(createInvestmentData("", "", "", "", "", "", "", 0))
    }
    setRows(res as InvestInfoData[]);

    let r =
      findInvestmentListQueryResult.data?.QueryInvestmentListByCompanyID;

    if (r === null) {
      setIsExist(false)
    }

    let totalCount =
      findInvestmentListQueryResult.data?.QueryInvestmentListByCompanyID
        ?.totalCount;
    setRounds(totalCount);
    setTotalPageCount(Math.ceil(totalCount / 3));
  }, [findInvestmentListQueryResult]);

  return (
    <div>
      <Grid item xs={12} className={isExist === true ? classes.container_investment : classes.noDisplay}>
        <Grid item xs={12} className={classes.investment}>
          <Box className={classes.financing_title}>投资交易</Box>
          <Box
            className={classes.financing_total_amount}
            display="flex"
            flexDirection="row"
          >
            <Box className={classes.amount1}>
              共
              <span
                style={{ color: "#333", marginRight: "3px", marginLeft: "3px" }}
              >
                {rounds}
              </span>
              笔投资
            </Box>
          </Box>
          <TableContainer component={Paper} style={{ boxShadow: "none" }}>
            <Table className={classes.table} aria-label="simple table">
              <TableHead style={{ background: "#FAFAFA" }}>
                <TableRow>
                  <TableCell className={classes.investment_founded_at_style}>
                    日期
                  </TableCell>
                  <TableCell
                    className={classes.investment_object_style}
                    align="left"
                  >
                    投资对象
                  </TableCell>
                  <TableCell
                    className={classes.company_category_style}
                    align="left"
                  >
                    企业类别
                  </TableCell>
                  <TableCell
                    className={classes.investment_financing_type_style}
                    align="left"
                  >
                    融资类型
                  </TableCell>
                  <TableCell
                    style={{ width: "700px" }}
                    align="right"
                  ></TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {rows?.map((row, index) => (
                  <TableRow key={row.fundedAt}>
                    <TableCell
                      className={classes.investment_founded_at_style}
                      component="th"
                      scope="row"
                    >
                      {row.fundedAt}
                    </TableCell>
                    <TableCell
                      className={classes.investment_object_style}
                      align="left"
                    >
                      <LinktoCompanyPage
                        companyID={row.fundedCompanyID}
                        companyName={row.fundedCompanyName as string}
                      ></LinktoCompanyPage>
                    </TableCell>
                    <TableCell
                      className={classes.company_category_style}
                      align="left"
                    >
                      {row.categoryCode}
                    </TableCell>
                    <TableCell
                      className={classes.investment_financing_type_style}
                      align="left"
                    >
                      <LinktoFundedType
                        companyID={Base64.encode(row.fundedCompanyID)}
                        fundingRoundType={row.fundingRoundType as string}
                        fundedAt={row.fundedAt as string}
                        raisedAmountUsd={row.raisedAmountUsd as number}
                      ></LinktoFundedType>
                    </TableCell>
                    <TableCell
                      style={{ width: "700px" }}
                      align="right"
                    ></TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
            <Box className={classes.pagination}>
              <Pagination
                count={totalPageCount}
                page={page}
                shape="rounded"
                onChange={handleChange}
                siblingCount={1}
                boundaryCount={1}
              />
            </Box>
          </TableContainer>
        </Grid>
      </Grid>
    </div>
  );
}
