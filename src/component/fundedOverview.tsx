import React, { useState, useEffect } from 'react';
import Grid from "@material-ui/core/Grid";
import { Box } from "@material-ui/core";
import { useFindFinancingOverviewQuery } from '../generated/graphql';
import Base64 from "base-64";
import { useStyles } from "./componentStyle"

interface FundedOverview {
    id: string,
    type: string,
    at: string,
    usd: number,
}

function tranNumber(num: number | undefined, point: number) {
    if (typeof num === "undefined" || num === 0) {
        return "-"
    }
    let n = num as number + ""
    let numStr = n.toString().split('.')[0]
    if (numStr.length < 5) {
        return numStr;
    } else if (numStr.length >= 5 && numStr.length <= 8) {
        let decimal = numStr.substring(numStr.length - 4, numStr.length - 4 + point)
        return Math.floor(num / 10000) + '.' + decimal + '万'
    } else if (numStr.length > 8) {
        let decimal = numStr.substring(numStr.length - 8, numStr.length - 8 + point);
        return Math.floor(num / 100000000) + '.' + decimal + '亿'
    }
}

export default function FundedOverview(props: FundedOverview) {
    const classes = useStyles();
    const [companyName, setcompanyName] = useState("企查查")

    const [fundingRoundType, setFundingRoundType] = useState("A")
    const [fundedAt, setFundedAt] = useState("2008-1-3")
    const [raisedAmountUsd, setRaisedAmountUsd] = useState(1)

    const [findFinancingOverviewQueryResult] = useFindFinancingOverviewQuery({
        variables: {
            id: Base64.decode(props.id),
            type: props.type,
            at: props.at,
            usd: props.usd,
        },
    });

    useEffect(() => {
        let res = findFinancingOverviewQueryResult.data?.QueryFinancingOverviewByFinancingIndex;
        setcompanyName(res?.companyInfo?.companyName as string);
        setFundingRoundType(res?.financingInfo?.fundingRoundType as string);
        setFundedAt(res?.financingInfo?.fundedAt as string);
        setRaisedAmountUsd(res?.financingInfo?.raisedAmountUsd as number);
    }, [findFinancingOverviewQueryResult])

    return (
        <div className={classes.root}>
            <Grid container>
                <Grid item xs={12} className={classes.container_overview}>
                    <Grid item xs={12} className={classes.overview}>
                        <Box className={classes.overview_title}>概览</Box>
                        <Box className={classes.overview_info} display="flex" flexDirection="row">
                            <Box className={classes.infoleft} display="flex" flexDirection="row">
                                <Box className={classes.info_left1}>
                                    <Box className={classes.infotext}>融资方</Box>
                                    <Box className={classes.infotext}>交易日期</Box>
                                    <Box className={classes.infotext}>交易类型</Box>
                                </Box>
                                <Box className={classes.info_left2}>
                                    <Box className={classes.infotext}>{companyName}</Box>
                                    <Box className={classes.infotext}>{fundedAt}</Box>
                                    <Box className={classes.infotext}>{fundingRoundType}</Box>
                                </Box>
                            </Box>
                            <Box className={classes.inforight} display="flex" flexDirection="row">
                                <Box className={classes.info_right1} >
                                    <Box className={classes.infotext}>融资金额</Box>
                                </Box>
                                <Box className={classes.info_right2} >
                                    <Box className={classes.infotext}>{tranNumber(raisedAmountUsd, 2)}</Box>
                                </Box>
                            </Box>
                        </Box>
                    </Grid>
                </Grid>
            </Grid>
        </div>
    )
}