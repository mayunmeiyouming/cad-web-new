import React, { useEffect, useState } from "react";
import { NavLink as Link } from 'react-router-dom'
import Base64 from "base-64";
import { useFindFinancingOverviewQuery } from '../generated/graphql';

interface LinktoFundedProps {
  companyID: string;
  fundingRoundType: string;
  fundedAt: string;
  raisedAmountUsd: number;
}

export default function LinktoFundedType(props: LinktoFundedProps) {
  let params = props.companyID + "/" + props.fundingRoundType + "/" + props.fundedAt + "/" + props.raisedAmountUsd
  let obj = {
    pathname: "/FundedPage/" + params,
  }
  const [isExist, setIsExist] = useState(true)
  const [findFinancingOverviewQueryResult] = useFindFinancingOverviewQuery({
    variables: {
      id: Base64.decode(props.companyID),
      type: props.fundingRoundType,
      at: props.fundedAt,
      usd: props.raisedAmountUsd,
    },
  });

  useEffect(() => {
    let res =
      findFinancingOverviewQueryResult.data?.QueryFinancingOverviewByFinancingIndex;

    if (res === null) {
      setIsExist(false)
    }
  }, [findFinancingOverviewQueryResult])

  function handle() {
    if (isExist) {
      return (
        <div>
          <Link to={obj} style={{ textDecoration: "none", color: "#3672EC" }}>{props.fundingRoundType}</Link>
        </div>
      );
    } else {
      return (
        <div style={{ display: "inline", marginRight: 10 }} >
          {props.fundingRoundType}
        </div>
      )
    }
  }

  return handle()
}
