import React, { useState, useEffect } from "react";
import "./App.css";
import { makeStyles, createStyles, Theme } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import Autocomplete from "@material-ui/lab/Autocomplete";
import { useFindCompanyListQuery } from "./generated/graphql";
// import { Link } from "react-router-dom";
import Base64 from 'base-64';
// import {withRouter} from 'react-router-dom';
import searchLogo from './img/search_logo.png'

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      flexGrow: 1,
    },
    box: {
      height: 240,
    },
    paper: {
      padding: theme.spacing(2),
      textAlign: "center",
      color: theme.palette.text.secondary,
    },
    searchInput: {
      borderRadius: 50,
      width: 200,
    },
    img: {
      paddingBottom: "40px",
      margin: "auto",
    },
  })
);

interface LinkTo {
  pathname: string
  search: string
  query: {}
  state: {}
}

interface SearchPageProps {
  history: {
    push: (path: string) => void
  }
}

export default function SearchPage(props: SearchPageProps) {
  const classes = useStyles();
  const [companyName, setCompanyName] = useState<string[]>([]);
  const [value] = useState<string | undefined>("");
  const [inputValue, setInputValue] = useState("");

  const [findCompanyListQueryResult] = useFindCompanyListQuery({
    variables: {
      name: inputValue,
    },
    pause: false,
  });

  useEffect(() => {
    let res =
      findCompanyListQueryResult.data?.QueryCompanyListByCompanyName
        ?.companyInfo;
    let newCompanyNameList: string[] = [];
    res?.map((value) => {
      newCompanyNameList.push(value?.companyName as string);
      return value?.companyName;
    });
    setTimeout(() => {
      setCompanyName(newCompanyNameList as string[]);
    }, 0)

  }, [findCompanyListQueryResult]);

  function handleSearchInputChange(value: string) {
    setTimeout(() => {
      setInputValue(value);
    }, 0)
    // executeFindCompanyListQuery();
    console.log("SearchInputChange");
  }

  function handleSearchButtonClick() {
    // executeFindCompanyListQuery();
    let res =
      findCompanyListQueryResult.data?.QueryCompanyListByCompanyName
        ?.companyInfo;
    let id = ""
    res?.map((value) => {
      if (inputValue === value?.companyName) {
        id = value.companyID as string
      }
      return value?.companyName;
    });
    if (id === "") {
      alert("没有该公司")
      return
    }
    id = Base64.encode(id)
    props.history.push("/CompanyPage/" + id);

    console.log("hello world");
  }

  return (
    <div className={classes.root}>
      <Grid container>
        <Grid item xs={12} className={classes.box}></Grid>

        <Grid item md={5} sm={4}  xs={3}></Grid>
        <Grid item md={2} sm={4}  xs={6}>
          <img alt="" className={classes.img} src={searchLogo} />
        </Grid>
        <Grid item md={5} sm={4}  xs={3}></Grid>

        <Grid item md={4} xs={1}></Grid>
        <Grid item md={4} xs={10}>
          <Autocomplete
            id="custom-input-demo"
            getOptionLabel={(option) =>
              typeof option === "string" ? option : "null"
            }
            filterOptions={(x) => x}
            options={companyName}
            autoComplete
            includeInputInList
            filterSelectedOptions
            value={value}
            onInputChange={(event, newInputValue) => {
              handleSearchInputChange(newInputValue);
              console.log(newInputValue);
            }}
            renderInput={(params) => (
              <div ref={params.InputProps.ref}>
                <input
                  id="searchInput"
                  style={{
                    outline: 0,
                    borderRadius: "50px 0 0 50px",
                    width: "65%",
                    height: 18,
                    padding: "12px 16px",
                    fontSize: "16px",
                    margin: 0,
                    verticalAlign: "top",
                    boxShadow: "none",
                    border: "2px solid #c4c7ce",
                    background: "#fff",
                    color: "#222",
                    overflow: "hidden",
                  }}
                  type="text"
                  onKeyUp={(e) => {
                    console.log(e.keyCode)
                    if (e.keyCode === 13) {
                      handleSearchButtonClick();
                      // alert(`按了回车键，msg值为：${e.currentTarget.value}`)
                    }
                  }}
                  {...params.inputProps}
                />
                <input
                  style={{
                    borderRadius: "0 50px 50px 0",
                    backgroundColor: "#4e6ef2",
                    fontWeight: 400,
                    color: "#fff",
                    boxShadow: "none",
                    cursor: "pointer",
                    border: "none",
                    width: "20%",
                    height: "46px",
                    lineHeight: "46px",
                    fontSize: "17px",
                    padding: 0,
                  }}
                  type="button"
                  value="搜索"
                  onClick={() => {
                    handleSearchButtonClick();
                  }}
                />
              </div>
            )}
          // renderOption={(option) => {
          //   return (
          //     <Grid container alignItems="center">
          //       <Grid item xs>
          //         {companyName.map((part, index) => ({ part }))}
          //       </Grid>
          //     </Grid>
          //   );
          // }}
          />
        </Grid>
        <Grid item md={4} xs={1}></Grid>
      </Grid>
    </div>
  );
}
