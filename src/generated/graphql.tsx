import gql from 'graphql-tag';
import * as Urql from 'urql';
export type Maybe<T> = T | null;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type Omit<T, K extends keyof T> = Pick<T, Exclude<keyof T, K>>;
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
  MaybeInt32: any;
  MaybeString: any;
  Int32: any;
  Int64: any;
  MaybeBool: any;
  MaybeInt64: any;
  Timestamp: any;
};

export type FinancingInfo = {
  __typename?: 'FinancingInfo';
  companyID?: Maybe<Scalars['String']>;
  fundingRoundType?: Maybe<Scalars['String']>;
  fundedAt?: Maybe<Scalars['String']>;
  raisedAmountUsd?: Maybe<Scalars['Int64']>;
  companyName?: Maybe<Scalars['String']>;
};

export type Query = {
  __typename?: 'Query';
  healthcheck: Scalars['String'];
  QueryCompanyListByCompanyName?: Maybe<CompanyInfoResponse>;
  QueryCompanyOverviewByCompanyID?: Maybe<CompanyOverviewResponse>;
  QueryFinancingListByCompanyID?: Maybe<FinancingDealConnection>;
  QueryInvestmentListByCompanyID?: Maybe<InvestmentTransactionConnection>;
  QueryExternalAcquisitionListByCompanyID?: Maybe<ExternalAcquisitionConnection>;
  QueryMergerInformationByCompanyID?: Maybe<MergerInfoResponse>;
  QueryFinancingOverviewByFinancingIndex?: Maybe<FinancingOverviewResponse>;
  QueryFinancingInvestorListByFinancingIndex?: Maybe<InvestorListConnection>;
};


export type QueryQueryCompanyListByCompanyNameArgs = {
  companyName: Scalars['String'];
};


export type QueryQueryCompanyOverviewByCompanyIdArgs = {
  companyID: Scalars['String'];
};


export type QueryQueryFinancingListByCompanyIdArgs = {
  first?: Maybe<Scalars['Int64']>;
  after?: Maybe<Scalars['Int64']>;
  last?: Maybe<Scalars['Int64']>;
  before?: Maybe<Scalars['Int64']>;
  companyID: Scalars['String'];
};


export type QueryQueryInvestmentListByCompanyIdArgs = {
  first?: Maybe<Scalars['Int64']>;
  after?: Maybe<Scalars['Int64']>;
  last?: Maybe<Scalars['Int64']>;
  before?: Maybe<Scalars['Int64']>;
  companyID: Scalars['String'];
};


export type QueryQueryExternalAcquisitionListByCompanyIdArgs = {
  first?: Maybe<Scalars['Int64']>;
  after?: Maybe<Scalars['Int64']>;
  last?: Maybe<Scalars['Int64']>;
  before?: Maybe<Scalars['Int64']>;
  companyID: Scalars['String'];
};


export type QueryQueryMergerInformationByCompanyIdArgs = {
  companyID: Scalars['String'];
};


export type QueryQueryFinancingOverviewByFinancingIndexArgs = {
  companyID: Scalars['String'];
  fundingRoundType: Scalars['String'];
  fundedAt: Scalars['String'];
  raisedAmountUsd: Scalars['Int64'];
};


export type QueryQueryFinancingInvestorListByFinancingIndexArgs = {
  first?: Maybe<Scalars['Int64']>;
  after?: Maybe<Scalars['Int64']>;
  last?: Maybe<Scalars['Int64']>;
  before?: Maybe<Scalars['Int64']>;
  companyID: Scalars['String'];
  fundingRoundType: Scalars['String'];
  fundedAt: Scalars['String'];
  raisedAmountUsd: Scalars['Int64'];
};


export type CompanyOverviewResponse = {
  __typename?: 'CompanyOverviewResponse';
  city?: Maybe<Scalars['String']>;
  foundedAt?: Maybe<Scalars['String']>;
  statusCode?: Maybe<Scalars['String']>;
  foundingRounds?: Maybe<Scalars['Int64']>;
  firstFunded?: Maybe<FinancingInfo>;
  lastFunded?: Maybe<FinancingInfo>;
  fundingTotalUSD?: Maybe<Scalars['Int64']>;
  companyName?: Maybe<Scalars['String']>;
};

export type FinancingDealEdge = {
  __typename?: 'FinancingDealEdge';
  node: FinancingDeal;
  cursor: Scalars['String'];
};

export type FinancingDealConnection = {
  __typename?: 'FinancingDealConnection';
  totalCount: Scalars['Int64'];
  nodes: Array<FinancingDeal>;
  edges: Array<FinancingDealEdge>;
  pageInfo: PageInfo;
};

export type InvestmentTransactionEdge = {
  __typename?: 'InvestmentTransactionEdge';
  node?: Maybe<InvestmentTransaction>;
  cursor?: Maybe<Scalars['String']>;
};

export type Investor = {
  __typename?: 'Investor';
  companyInfo?: Maybe<CompanyInfo>;
};

export type ExternalAcquisitionConnection = {
  __typename?: 'ExternalAcquisitionConnection';
  totalCount?: Maybe<Scalars['Int64']>;
  nodes?: Maybe<Array<Maybe<ExternalAcquisition>>>;
  edges?: Maybe<Array<Maybe<ExternalAcquisitionEdge>>>;
  pageInfo?: Maybe<PageInfo>;
};

export type MergerInfoResponse = {
  __typename?: 'MergerInfoResponse';
  acquirerInfo?: Maybe<CompanyInfo>;
  acquiredAt?: Maybe<Scalars['String']>;
  priceAmount?: Maybe<Scalars['Int64']>;
};

export type PageInfo = {
  __typename?: 'PageInfo';
  hasPreviousPage: Scalars['Boolean'];
  hasNextPage: Scalars['Boolean'];
  startCursor?: Maybe<Scalars['MaybeString']>;
  endCursor?: Maybe<Scalars['MaybeString']>;
};




export type CompanyInfoResponse = {
  __typename?: 'CompanyInfoResponse';
  companyInfo?: Maybe<Array<Maybe<CompanyInfo>>>;
};

export type ExternalAcquisitionEdge = {
  __typename?: 'ExternalAcquisitionEdge';
  node?: Maybe<ExternalAcquisition>;
  cursor?: Maybe<Scalars['String']>;
};

export type Mutation = {
  __typename?: 'Mutation';
  healthcheck: Scalars['String'];
};




export type InvestmentTransactionConnection = {
  __typename?: 'InvestmentTransactionConnection';
  totalCount?: Maybe<Scalars['Int64']>;
  nodes?: Maybe<Array<Maybe<InvestmentTransaction>>>;
  edges?: Maybe<Array<Maybe<InvestmentTransactionEdge>>>;
  pageInfo?: Maybe<PageInfo>;
};

export type InvestorEdge = {
  __typename?: 'InvestorEdge';
  node?: Maybe<Investor>;
  cursor?: Maybe<Scalars['String']>;
};

export type CompanyInfo = {
  __typename?: 'CompanyInfo';
  companyName?: Maybe<Scalars['String']>;
  companyID?: Maybe<Scalars['String']>;
  categoryCode?: Maybe<Scalars['String']>;
};

export type FinancingDeal = {
  __typename?: 'FinancingDeal';
  financingInfo: FinancingInfo;
  investors: Array<CompanyInfo>;
};

export type InvestmentTransaction = {
  __typename?: 'InvestmentTransaction';
  investorInfo?: Maybe<CompanyInfo>;
  financingInfo?: Maybe<FinancingInfo>;
};

export type ExternalAcquisition = {
  __typename?: 'ExternalAcquisition';
  companyInfo?: Maybe<CompanyInfo>;
  acquiredAt?: Maybe<Scalars['String']>;
  priceAmount?: Maybe<Scalars['Int64']>;
};

export type FinancingOverviewResponse = {
  __typename?: 'FinancingOverviewResponse';
  companyInfo?: Maybe<CompanyInfo>;
  financingInfo?: Maybe<FinancingInfo>;
};

export type InvestorListConnection = {
  __typename?: 'InvestorListConnection';
  totalCount?: Maybe<Scalars['Int64']>;
  nodes?: Maybe<Array<Maybe<Investor>>>;
  edges?: Maybe<Array<Maybe<InvestorEdge>>>;
  pageInfo?: Maybe<PageInfo>;
};

export type FindCompanyListQueryVariables = Exact<{
  name: Scalars['String'];
}>;


export type FindCompanyListQuery = (
  { __typename?: 'Query' }
  & { QueryCompanyListByCompanyName?: Maybe<(
    { __typename?: 'CompanyInfoResponse' }
    & { companyInfo?: Maybe<Array<Maybe<(
      { __typename?: 'CompanyInfo' }
      & Pick<CompanyInfo, 'companyID' | 'companyName' | 'categoryCode'>
    )>>> }
  )> }
);

export type FindCompanyOverviewQueryVariables = Exact<{
  id: Scalars['String'];
}>;


export type FindCompanyOverviewQuery = (
  { __typename?: 'Query' }
  & { QueryCompanyOverviewByCompanyID?: Maybe<(
    { __typename?: 'CompanyOverviewResponse' }
    & Pick<CompanyOverviewResponse, 'city' | 'foundedAt' | 'statusCode' | 'foundingRounds' | 'fundingTotalUSD' | 'companyName'>
    & { firstFunded?: Maybe<(
      { __typename?: 'FinancingInfo' }
      & Pick<FinancingInfo, 'companyID' | 'fundingRoundType' | 'fundedAt' | 'raisedAmountUsd'>
    )>, lastFunded?: Maybe<(
      { __typename?: 'FinancingInfo' }
      & Pick<FinancingInfo, 'companyID' | 'fundingRoundType' | 'fundedAt' | 'raisedAmountUsd'>
    )> }
  )> }
);

export type FindMergerInfomationQueryVariables = Exact<{
  id: Scalars['String'];
}>;


export type FindMergerInfomationQuery = (
  { __typename?: 'Query' }
  & { QueryMergerInformationByCompanyID?: Maybe<(
    { __typename?: 'MergerInfoResponse' }
    & Pick<MergerInfoResponse, 'acquiredAt' | 'priceAmount'>
    & { acquirerInfo?: Maybe<(
      { __typename?: 'CompanyInfo' }
      & Pick<CompanyInfo, 'companyID' | 'companyName' | 'categoryCode'>
    )> }
  )> }
);

export type FindFinancingListQueryVariables = Exact<{
  first?: Maybe<Scalars['Int64']>;
  after?: Maybe<Scalars['Int64']>;
  companyID: Scalars['String'];
}>;


export type FindFinancingListQuery = (
  { __typename?: 'Query' }
  & { QueryFinancingListByCompanyID?: Maybe<(
    { __typename?: 'FinancingDealConnection' }
    & Pick<FinancingDealConnection, 'totalCount'>
    & { nodes: Array<(
      { __typename?: 'FinancingDeal' }
      & { investors: Array<(
        { __typename?: 'CompanyInfo' }
        & Pick<CompanyInfo, 'companyID' | 'companyName' | 'categoryCode'>
      )>, financingInfo: (
        { __typename?: 'FinancingInfo' }
        & Pick<FinancingInfo, 'companyID' | 'fundingRoundType' | 'fundedAt' | 'raisedAmountUsd'>
      ) }
    )>, edges: Array<(
      { __typename?: 'FinancingDealEdge' }
      & { node: (
        { __typename?: 'FinancingDeal' }
        & { investors: Array<(
          { __typename?: 'CompanyInfo' }
          & Pick<CompanyInfo, 'companyID' | 'companyName' | 'categoryCode'>
        )>, financingInfo: (
          { __typename?: 'FinancingInfo' }
          & Pick<FinancingInfo, 'companyID' | 'fundingRoundType' | 'fundedAt' | 'raisedAmountUsd'>
        ) }
      ) }
    )>, pageInfo: (
      { __typename?: 'PageInfo' }
      & Pick<PageInfo, 'endCursor' | 'startCursor' | 'hasNextPage' | 'hasPreviousPage'>
    ) }
  )> }
);

export type FindFinancingOverviewQueryVariables = Exact<{
  id: Scalars['String'];
  type: Scalars['String'];
  at: Scalars['String'];
  usd: Scalars['Int64'];
}>;


export type FindFinancingOverviewQuery = (
  { __typename?: 'Query' }
  & { QueryFinancingOverviewByFinancingIndex?: Maybe<(
    { __typename?: 'FinancingOverviewResponse' }
    & { companyInfo?: Maybe<(
      { __typename?: 'CompanyInfo' }
      & Pick<CompanyInfo, 'companyName' | 'companyID' | 'categoryCode'>
    )>, financingInfo?: Maybe<(
      { __typename?: 'FinancingInfo' }
      & Pick<FinancingInfo, 'companyID' | 'fundingRoundType' | 'fundedAt' | 'raisedAmountUsd'>
    )> }
  )> }
);

export type FindInvestmentListQueryVariables = Exact<{
  first?: Maybe<Scalars['Int64']>;
  after?: Maybe<Scalars['Int64']>;
  companyID: Scalars['String'];
}>;


export type FindInvestmentListQuery = (
  { __typename?: 'Query' }
  & { QueryInvestmentListByCompanyID?: Maybe<(
    { __typename?: 'InvestmentTransactionConnection' }
    & Pick<InvestmentTransactionConnection, 'totalCount'>
    & { nodes?: Maybe<Array<Maybe<(
      { __typename?: 'InvestmentTransaction' }
      & { investorInfo?: Maybe<(
        { __typename?: 'CompanyInfo' }
        & Pick<CompanyInfo, 'companyID' | 'companyName' | 'categoryCode'>
      )>, financingInfo?: Maybe<(
        { __typename?: 'FinancingInfo' }
        & Pick<FinancingInfo, 'companyID' | 'fundingRoundType' | 'fundedAt' | 'raisedAmountUsd' | 'companyName'>
      )> }
    )>>>, edges?: Maybe<Array<Maybe<(
      { __typename?: 'InvestmentTransactionEdge' }
      & { node?: Maybe<(
        { __typename?: 'InvestmentTransaction' }
        & { investorInfo?: Maybe<(
          { __typename?: 'CompanyInfo' }
          & Pick<CompanyInfo, 'companyID' | 'companyName' | 'categoryCode'>
        )>, financingInfo?: Maybe<(
          { __typename?: 'FinancingInfo' }
          & Pick<FinancingInfo, 'companyID' | 'fundingRoundType' | 'fundedAt' | 'raisedAmountUsd' | 'companyName'>
        )> }
      )> }
    )>>>, pageInfo?: Maybe<(
      { __typename?: 'PageInfo' }
      & Pick<PageInfo, 'startCursor' | 'endCursor' | 'hasNextPage' | 'hasPreviousPage'>
    )> }
  )> }
);

export type FindAcquisitionListQueryVariables = Exact<{
  first?: Maybe<Scalars['Int64']>;
  after?: Maybe<Scalars['Int64']>;
  companyID: Scalars['String'];
}>;


export type FindAcquisitionListQuery = (
  { __typename?: 'Query' }
  & { QueryExternalAcquisitionListByCompanyID?: Maybe<(
    { __typename?: 'ExternalAcquisitionConnection' }
    & Pick<ExternalAcquisitionConnection, 'totalCount'>
    & { nodes?: Maybe<Array<Maybe<(
      { __typename?: 'ExternalAcquisition' }
      & Pick<ExternalAcquisition, 'acquiredAt' | 'priceAmount'>
      & { companyInfo?: Maybe<(
        { __typename?: 'CompanyInfo' }
        & Pick<CompanyInfo, 'companyID' | 'companyName' | 'categoryCode'>
      )> }
    )>>>, edges?: Maybe<Array<Maybe<(
      { __typename?: 'ExternalAcquisitionEdge' }
      & Pick<ExternalAcquisitionEdge, 'cursor'>
      & { node?: Maybe<(
        { __typename?: 'ExternalAcquisition' }
        & Pick<ExternalAcquisition, 'acquiredAt' | 'priceAmount'>
        & { companyInfo?: Maybe<(
          { __typename?: 'CompanyInfo' }
          & Pick<CompanyInfo, 'companyID' | 'companyName' | 'categoryCode'>
        )> }
      )> }
    )>>>, pageInfo?: Maybe<(
      { __typename?: 'PageInfo' }
      & Pick<PageInfo, 'endCursor' | 'startCursor' | 'hasNextPage' | 'hasPreviousPage'>
    )> }
  )> }
);

export type FindFinancingInvestorListQueryVariables = Exact<{
  first?: Maybe<Scalars['Int64']>;
  after?: Maybe<Scalars['Int64']>;
  id: Scalars['String'];
  type: Scalars['String'];
  at: Scalars['String'];
  usd: Scalars['Int64'];
}>;


export type FindFinancingInvestorListQuery = (
  { __typename?: 'Query' }
  & { QueryFinancingInvestorListByFinancingIndex?: Maybe<(
    { __typename?: 'InvestorListConnection' }
    & Pick<InvestorListConnection, 'totalCount'>
    & { nodes?: Maybe<Array<Maybe<(
      { __typename?: 'Investor' }
      & { companyInfo?: Maybe<(
        { __typename?: 'CompanyInfo' }
        & Pick<CompanyInfo, 'companyID' | 'companyName' | 'categoryCode'>
      )> }
    )>>>, pageInfo?: Maybe<(
      { __typename?: 'PageInfo' }
      & Pick<PageInfo, 'endCursor' | 'startCursor' | 'hasNextPage' | 'hasPreviousPage'>
    )> }
  )> }
);


export const FindCompanyListDocument = gql`
    query findCompanyList($name: String!) {
  QueryCompanyListByCompanyName(companyName: $name) {
    companyInfo {
      companyID
      companyName
      categoryCode
    }
  }
}
    `;

export function useFindCompanyListQuery(options: Omit<Urql.UseQueryArgs<FindCompanyListQueryVariables>, 'query'> = {}) {
  return Urql.useQuery<FindCompanyListQuery>({ query: FindCompanyListDocument, ...options });
};
export const FindCompanyOverviewDocument = gql`
    query findCompanyOverview($id: String!) {
  QueryCompanyOverviewByCompanyID(companyID: $id) {
    city
    foundedAt
    statusCode
    foundingRounds
    firstFunded {
      companyID
      fundingRoundType
      fundedAt
      raisedAmountUsd
    }
    lastFunded {
      companyID
      fundingRoundType
      fundedAt
      raisedAmountUsd
    }
    fundingTotalUSD
    companyName
  }
}
    `;

export function useFindCompanyOverviewQuery(options: Omit<Urql.UseQueryArgs<FindCompanyOverviewQueryVariables>, 'query'> = {}) {
  return Urql.useQuery<FindCompanyOverviewQuery>({ query: FindCompanyOverviewDocument, ...options });
};
export const FindMergerInfomationDocument = gql`
    query findMergerInfomation($id: String!) {
  QueryMergerInformationByCompanyID(companyID: $id) {
    acquiredAt
    priceAmount
    acquirerInfo {
      companyID
      companyName
      categoryCode
    }
  }
}
    `;

export function useFindMergerInfomationQuery(options: Omit<Urql.UseQueryArgs<FindMergerInfomationQueryVariables>, 'query'> = {}) {
  return Urql.useQuery<FindMergerInfomationQuery>({ query: FindMergerInfomationDocument, ...options });
};
export const FindFinancingListDocument = gql`
    query findFinancingList($first: Int64, $after: Int64, $companyID: String!) {
  QueryFinancingListByCompanyID(first: $first, after: $after, companyID: $companyID) {
    totalCount
    nodes {
      investors {
        companyID
        companyName
        categoryCode
      }
      financingInfo {
        companyID
        fundingRoundType
        fundedAt
        raisedAmountUsd
      }
    }
    edges {
      node {
        investors {
          companyID
          companyName
          categoryCode
        }
        financingInfo {
          companyID
          fundingRoundType
          fundedAt
          raisedAmountUsd
        }
      }
    }
    pageInfo {
      endCursor
      startCursor
      hasNextPage
      hasPreviousPage
    }
  }
}
    `;

export function useFindFinancingListQuery(options: Omit<Urql.UseQueryArgs<FindFinancingListQueryVariables>, 'query'> = {}) {
  return Urql.useQuery<FindFinancingListQuery>({ query: FindFinancingListDocument, ...options });
};
export const FindFinancingOverviewDocument = gql`
    query findFinancingOverview($id: String!, $type: String!, $at: String!, $usd: Int64!) {
  QueryFinancingOverviewByFinancingIndex(companyID: $id, fundingRoundType: $type, fundedAt: $at, raisedAmountUsd: $usd) {
    companyInfo {
      companyName
      companyID
      categoryCode
    }
    financingInfo {
      companyID
      fundingRoundType
      fundedAt
      raisedAmountUsd
    }
  }
}
    `;

export function useFindFinancingOverviewQuery(options: Omit<Urql.UseQueryArgs<FindFinancingOverviewQueryVariables>, 'query'> = {}) {
  return Urql.useQuery<FindFinancingOverviewQuery>({ query: FindFinancingOverviewDocument, ...options });
};
export const FindInvestmentListDocument = gql`
    query findInvestmentList($first: Int64, $after: Int64, $companyID: String!) {
  QueryInvestmentListByCompanyID(first: $first, after: $after, companyID: $companyID) {
    totalCount
    nodes {
      investorInfo {
        companyID
        companyName
        categoryCode
      }
      financingInfo {
        companyID
        fundingRoundType
        fundedAt
        raisedAmountUsd
        companyName
      }
    }
    edges {
      node {
        investorInfo {
          companyID
          companyName
          categoryCode
        }
        financingInfo {
          companyID
          fundingRoundType
          fundedAt
          raisedAmountUsd
          companyName
        }
      }
    }
    pageInfo {
      startCursor
      endCursor
      hasNextPage
      hasPreviousPage
    }
  }
}
    `;

export function useFindInvestmentListQuery(options: Omit<Urql.UseQueryArgs<FindInvestmentListQueryVariables>, 'query'> = {}) {
  return Urql.useQuery<FindInvestmentListQuery>({ query: FindInvestmentListDocument, ...options });
};
export const FindAcquisitionListDocument = gql`
    query findAcquisitionList($first: Int64, $after: Int64, $companyID: String!) {
  QueryExternalAcquisitionListByCompanyID(first: $first, after: $after, companyID: $companyID) {
    totalCount
    nodes {
      companyInfo {
        companyID
        companyName
        categoryCode
      }
      acquiredAt
      priceAmount
    }
    edges {
      node {
        companyInfo {
          companyID
          companyName
          categoryCode
        }
        acquiredAt
        priceAmount
      }
      cursor
    }
    pageInfo {
      endCursor
      startCursor
      hasNextPage
      hasPreviousPage
    }
  }
}
    `;

export function useFindAcquisitionListQuery(options: Omit<Urql.UseQueryArgs<FindAcquisitionListQueryVariables>, 'query'> = {}) {
  return Urql.useQuery<FindAcquisitionListQuery>({ query: FindAcquisitionListDocument, ...options });
};
export const FindFinancingInvestorListDocument = gql`
    query FindFinancingInvestorList($first: Int64, $after: Int64, $id: String!, $type: String!, $at: String!, $usd: Int64!) {
  QueryFinancingInvestorListByFinancingIndex(first: $first, after: $after, companyID: $id, fundingRoundType: $type, fundedAt: $at, raisedAmountUsd: $usd) {
    totalCount
    nodes {
      companyInfo {
        companyID
        companyName
        categoryCode
      }
    }
    pageInfo {
      endCursor
      startCursor
      hasNextPage
      hasPreviousPage
    }
  }
}
    `;

export function useFindFinancingInvestorListQuery(options: Omit<Urql.UseQueryArgs<FindFinancingInvestorListQueryVariables>, 'query'> = {}) {
  return Urql.useQuery<FindFinancingInvestorListQuery>({ query: FindFinancingInvestorListDocument, ...options });
};