import React, { useState, useEffect } from "react";
import Base64 from "base-64";
import Grid from "@material-ui/core/Grid";
import CompanyOverview from "./component/companyOverview";
import FundedInfo from "./component/fundedInfo";
import InvestInfo from "./component/investInfo";
import AcquisitionInfo from "./component/acquisitionInfo";
import MergeInfo from "./component/mergeInfo";
import FundedSearch from "./component/fundedSearch";
import { useFindCompanyOverviewQuery } from "./generated/graphql";
import { useStyles } from "./component/componentStyle"

interface CompanyPageProps {
  id: string;
  match: {
    params: {
      id: string;
    };
  };
}

export default function CompanyPage(props: CompanyPageProps) {
  const classes = useStyles();
  const [companyName, setCompanyName] = useState("企查查");
  const [findCompanyOverviewQueryResult] = useFindCompanyOverviewQuery({
    variables: {
      id: Base64.decode(props.match.params.id),
    },
  });

  useEffect(() => {
    let res =
      findCompanyOverviewQueryResult.data?.QueryCompanyOverviewByCompanyID;
    setCompanyName(res?.companyName as string);
  }, [findCompanyOverviewQueryResult]);

  return (
    <div>
      <Grid container spacing={2}>
        <Grid item xs={12}>
          <FundedSearch />
          <Grid item xs={12} className={classes.companyName} style={{ borderTop: "1px solid #fff" }}>
            {companyName}
          </Grid>
        </Grid>
        <Grid item xs={12} className={classes.container_overview}>
          <CompanyOverview id={props.match.params.id}></CompanyOverview>
        </Grid>
        <Grid item xs={12} className={classes.container_financing}>
          <FundedInfo id={props.match.params.id}></FundedInfo>
        </Grid>
        <Grid item xs={12} className={classes.container_investment}>
          <InvestInfo id={props.match.params.id}></InvestInfo>
        </Grid>
        <Grid item xs={12} className={classes.container_acquisition}>
          <AcquisitionInfo id={props.match.params.id}></AcquisitionInfo>
        </Grid>
        <Grid item xs={12} className={classes.container_mergeinfo}>
          <MergeInfo id={props.match.params.id}></MergeInfo>
        </Grid>
      </Grid>
    </div>
  );
}
